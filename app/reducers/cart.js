import {updateObject, createReducer} from '../helpers';

function add (state, action) {
    return [...state, action.item]
}

const cart = createReducer([], {
    'add' : add
});


export default cart;