import React from 'react';
import './../styles/Main.scss';
import Headline from './Headline';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as cartActions from '../../actions/cart';
import Test from './Test';

class Main extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
    }
    render() {
        console.log('props', this.props);
        return (<div className="container">

            <Test 
                cart={this.props.cart}
                addItem={this.props.actions.addToCart}/>
        </div>
        );
    }
}

function mapStateProps(state, props){
    return {
        cart : state.cart
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions : bindActionCreators(cartActions, dispatch)
    };
}
export default connect(mapStateProps, mapDispatchToProps)(Main);