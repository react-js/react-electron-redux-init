import React from 'react';
export default class Test extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            items : ['cats', 'origami', 'jazz', 'math rock']
        }
    }
 
    addItem(item) {
        this.props.addItem(item);
    }

    render(){
        console.log('test props', this.props);
        const items = this.state.items.map((item, idx) => {
            return <div key={idx}><button onClick={() => this.addItem(item)}>[+] {item}</button></div>
        })
        const cart = this.props.cart.map((item, idx) => {
            return <li key={idx}>
                <h4>{item}</h4>
            </li>
        })

        return (
            <div>
                <h2>Teste redux</h2>
                {items}
                
               <div style={{backgroundColor : '#ccc'}}>
                   <ul>{cart}</ul>
               </div>            
            </div>
        )
    }
}
