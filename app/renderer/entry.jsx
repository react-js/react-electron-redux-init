// Need both React and ReactDOM for the JSX transpiler.
import ReactDOM from 'react-dom';
import React from 'react';
import Main from './components/Main';
import {Provider} from 'react-redux';
import Store from '../store';

const StoreInstance = Store();

ReactDOM.render(
  <Provider store={StoreInstance}>
    <Main />
  </Provider>,
  document.getElementById('react-root')
);